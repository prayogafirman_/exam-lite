<?php

namespace App\Http\Middleware;

use Closure,Cookie;

class ExamMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Cookie::get('exam_data')!=null) {
            $exam_data = Cookie::get('exam_data');
            $exam_data = json_decode($exam_data,true);
            if ($exam_data['is_done']!=true) {
                return $next($request);
            }else{
                return redirect('analysis');
            }
        }else{
            return redirect('');
        }
    }
}
