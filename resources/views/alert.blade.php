<link rel="stylesheet" href="{{ asset('/assets/izitoast/css/iziToast.min.css') }}">
<script src="{{ asset('/assets/izitoast/js/iziToast.min.js') }}"></script>
<script>
	@if(session('success')!=null)
	iziToast.success({
		title: 'Berhasil',
		message: '{{ session('success') }}',
		position: 'topRight'
	});
	@endif

	@if(session('error')!=null)
	iziToast.error({
		title: 'Gagal',
		message: '{{ session('error') }}',
		position: 'topRight'
	});
	@endif

	@if(session('info')!=null)
	iziToast.info({
		title: 'Info',
		message: '{{ session('info') }}',
		position: 'topRight'
	});
	@endif

	@if($errors->any())
	@foreach($errors->all() as $e)
	iziToast.error({
		title: 'Gagal',
		message: '{{$e}}',
		position: 'topRight'
	});
	@endforeach
	@endif


</script>
