@extends('layout.index_layout')
@section('title','Exam-Lite | Insert questions')
@section('content')
<div class="content">
	<form action="{{ url('question/store') }}" method="post">
		@csrf
		<div class="panel-header bg-primary-gradient">
			<div class="page-inner py-5">
				<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
					<div>
						<h2 class="text-white pb-2 fw-bold">Insert Questions (Stored Question(s) : {{ count($stored_question) }})</h2>
						<h5 class="text-white op-7 mb-2">please insert at least one question</h5>
					</div>
					<div class="ml-md-auto py-2 py-md-0">
						<a href="{{ url('reset') }}" class="btn btn-white btn-border btn-round mr-2">Reset Exam</a>
						<button type="submit" class="btn btn-white btn-border btn-round mr-2">Store Question</button>
						<button type="button" data-toggle="modal" data-target="#start-exam" class="btn btn-secondary btn-round">Let's Start the Exam</button>
					</div>
				</div>
			</div>
		</div>
		<div class="page-inner mt--5">
			<div class="row mt--2">
				<div class="col-md-12">
					<div class="card full-height">
						<div class="card-body">
							<div class="row">
								<div class="col-3 col-md-3">
									<div class="nav flex-column nav-pills nav-secondary nav-pills-no-bd" id="v-pills-tab-without-border" role="tablist" aria-orientation="vertical">
										<a class="nav-link active" id="question0" data-toggle="pill" href="#form_question0" role="tab" aria-controls="form_question0" aria-selected="true">Question</a>
										<a class="nav-link" id="optiona0" data-toggle="pill" href="#form_optiona0" role="tab" aria-controls="form_optiona0" aria-selected="false">Option A</a>
										<a class="nav-link" id="optionb0" data-toggle="pill" href="#form_optionb0" role="tab" aria-controls="form_optionb0" aria-selected="false">Option B</a>
										<a class="nav-link" id="optionc0" data-toggle="pill" href="#form_optionc0" role="tab" aria-controls="form_optionc0" aria-selected="false">Option C</a>
										<a class="nav-link" id="optiond0" data-toggle="pill" href="#form_optiond0" role="tab" aria-controls="form_optiond0" aria-selected="false">Option D</a>
									</div>
								</div>
								<div class="col-9 col-md-9">
									<div class="tab-content" id="v-pills-without-border-tabContent">
										<div class="tab-pane fade show active" id="form_question0" role="tabpanel" aria-labelledby="question0">
											<div class="form-group">
												<label class="form-label">Anwer</label>
												<div class="selectgroup w-100">
													<label class="selectgroup-item">
														<input type="radio" name="answer" value="A" class="selectgroup-input" required="">
														<span class="selectgroup-button">A</span>
													</label>
													<label class="selectgroup-item">
														<input type="radio" name="answer" value="B" class="selectgroup-input" required="">
														<span class="selectgroup-button">B</span>
													</label>
													<label class="selectgroup-item">
														<input type="radio" name="answer" value="C" class="selectgroup-input" required="">
														<span class="selectgroup-button">C</span>
													</label>
													<label class="selectgroup-item">
														<input type="radio" name="answer" value="D" class="selectgroup-input" required="">
														<span class="selectgroup-button">D</span>
													</label>
												</div>
											</div>
											<textarea required="" name="question" placeholderText="lorem" class="froala form-control"></textarea>
										</div>
										<div class="tab-pane fade" id="form_optiona0" role="tabpanel" aria-labelledby="optiona0">
											<textarea required="" name="option_a" class="froala form-control"></textarea>
										</div>
										<div class="tab-pane fade" id="form_optionb0" role="tabpanel" aria-labelledby="optionb0">
											<textarea required="" name="option_b" class="froala form-control"></textarea>
										</div>
										<div class="tab-pane fade" id="form_optionc0" role="tabpanel" aria-labelledby="optionc0">
											<textarea required="" name="option_c" class="froala form-control"></textarea>
										</div>
										<div class="tab-pane fade" id="form_optiond0" role="tabpanel" aria-labelledby="optiond0">
											<textarea required="" name="option_d" class="froala form-control"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="start-exam">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Tambah Data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="{{ url('question/start-exam') }}" method="post" enctype="multipart/form-data">
				@csrf
				<div class="modal-body">
					<div class="form-group">
						<label for="">Limit Time (minute)</label>
						<input type="number" class="form-control" required="" name="time">
					</div>
				</div>
				<div class="modal-footer bg-whitesmoke br">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('froala/css/froala_editor.css') }}">
<link rel="stylesheet" href="{{ asset('froala/css/froala_style.css') }}">
<link rel="stylesheet" href="{{ asset('froala/css/plugins/code_view.css') }}">
<link rel="stylesheet" href="{{ asset('froala/css/plugins/draggable.css') }}">
<link rel="stylesheet" href="{{ asset('froala/css/plugins/colors.css') }}">
<link rel="stylesheet" href="{{ asset('froala/css/plugins/emoticons.css') }}">
<link rel="stylesheet" href="{{ asset('froala/css/plugins/image_manager.css') }}">
<link rel="stylesheet" href="{{ asset('froala/css/plugins/image.css') }}">
<link rel="stylesheet" href="{{ asset('froala/css/plugins/line_breaker.css') }}">
<link rel="stylesheet" href="{{ asset('froala/css/plugins/table.css') }}">
<link rel="stylesheet" href="{{ asset('froala/css/plugins/char_counter.css') }}">
<link rel="stylesheet" href="{{ asset('froala/css/plugins/video.css') }}">
<link rel="stylesheet" href="{{ asset('froala/css/plugins/fullscreen.css') }}">
<link rel="stylesheet" href="{{ asset('froala/css/plugins/file.css') }}">
<link rel="stylesheet" href="{{ asset('froala/css/plugins/quick_insert.css') }}">
<link rel="stylesheet" href="{{ asset('froala/css/plugins/help.css') }}">
<link rel="stylesheet" href="{{ asset('froala/css/third_party/spell_checker.css') }}">
<link rel="stylesheet" href="{{ asset('froala/css/plugins/special_characters.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
	.hide {
		display: none;
	}
</style>
@endsection

@section('js')
<script type="text/javascript"
src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
<script type="text/javascript"
src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>

<script type="text/javascript" src="{{ asset('froala/js/froala_editor.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/align.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/char_counter.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/code_beautifier.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/code_view.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/colors.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/draggable.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/emoticons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/entities.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/file.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/font_size.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/font_family.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/fullscreen.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/image.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/image_manager.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/line_breaker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/inline_style.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/link.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/lists.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/paragraph_format.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/paragraph_style.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/quick_insert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/quote.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/table.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/save.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/url.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/video.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/help.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/print.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/third_party/spell_checker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/special_characters.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/word_paste.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/lodash@4.17.15/lodash.min.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
<script src="{{ asset('') }}assets/sweetalert/sweetalert.min.js"></script>
<script>
	function confdel(id) {
		swal({
			title: 'Hapus data?',
			text: 'Semua data yang berhubungan dengan data ini akan ikut terhapus!',
			icon: 'warning',
			buttons: true,
			dangerMode: true,
		})
		.then((willDelete) => {
			if (willDelete) {
				$("#form-"+id).submit();
			}
		});
	}
	(function () {
		new FroalaEditor(".froala",{
			height:'400',
			theme : 'dark'
		})
	})()
</script>
@endsection