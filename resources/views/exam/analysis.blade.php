@extends('layout.index_layout')
@section("title','Exam-Lite | Exam's Result")
@section('content')
<div class="content">
	<form action="{{ url('question/store') }}" method="post">
		@csrf
		<div class="panel-header bg-primary-gradient">
			<div class="page-inner py-5">
				<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
					<div>
						<h2 class="text-white pb-2 fw-bold">Analysis</h2>
						<h5 class="text-white op-7 mb-2">here's your exam's result, congrats!</h5>
					</div>
					<div class="ml-md-auto py-2 py-md-0">
						<a href="{{ url('reset') }}" class="btn btn-white btn-border btn-round mr-2">Reset Exam</a>
					</div>
				</div>
			</div>
		</div>
		<div class="page-inner mt--5">
			<div class="row mt--2">
				<div class="col-md-12">
					<div class="card full-height">
						<div class="card-body">
							@php
							$answered = Cookie::get('answered');
							$answered = json_decode($answered,true);

							$list_question = Cookie::get('stored_question');
							$list_question = json_decode($list_question,false);
							@endphp
							@foreach ($list_question as $e)
							@php
							$opt  = "";
							if ($answered!=null) {
								foreach ($answered as $key) {
									if ($key['index']==$loop->index) {
										$class = "btn-success";
										$opt = $key['answer'];
									}
								}
							}
							@endphp
							<div class="card">
								<div class="card-body">
									{!! $e->question !!}
									<ol type="A">
										<li {{ $e->answer=='A' ? "class=betul" : '' }}>{!! $e->option_a !!}</li>
										<li {{ $e->answer=='B' ? "class=betul" : '' }}>{!! $e->option_b !!}</li>
										<li {{ $e->answer=='C' ? "class=betul" : '' }}>{!! $e->option_c !!}</li>
										<li {{ $e->answer=='D' ? "class=betul" : '' }}>{!! $e->option_d !!}</li>
									</ol>
									<b>Your answer : {{ $opt }}</b>
								</div>
							</div>
							<hr>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

@endsection

@section('css')
<style>
	.hide {
		display: none;
	}
	.betul{
		background-color: #31ce3636;
		border-radius: 10px;
	}
	ol li{
		padding-left: 10px;
	}
</style>
@endsection

@section('js')
@endsection